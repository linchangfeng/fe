//
//  CellInfo.m
//  Account
//
//  Created by Tracy on 16/2/27.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import "FECellInfo.h"

@implementation FECellInfo

+ (id)infoWithTitle:(NSString*)title tag:(NSInteger)tag {
    return [[FECellInfo alloc] initWithTitle:title tag:tag];
}

+ (id)infoWithTitle:(NSString*)title object:(id)object tag:(NSInteger)tag {
    return [[FECellInfo alloc] initWithTitle:title object:object tag:tag];
}

+ (id)infoWithTitle:(NSString *)title object:(id)object icon:(UIImage *)icon tag:(NSInteger)tag {
    return [[FECellInfo alloc] initWithTitle:title object:object icon:icon tag:tag];
}

+ (id)infoWithTitle:(NSString*)title icon:(UIImage*)icon tag:(NSInteger)tag {
    return [[FECellInfo alloc] initWithTitle:title object:nil icon:icon tag:tag];
}

- (id)initWithTitle:(NSString*)title tag:(NSInteger)tag {
    return [self initWithTitle:title object:nil icon:nil tag:tag];
}

- (id)initWithTitle:(NSString*)title object:(id)object tag:(NSInteger)tag {
    return [self initWithTitle:title object:object icon:nil tag:tag];
}

- (id)initWithTitle:(NSString*)title icon:(UIImage*)icon tag:(NSInteger)tag {
    return [self initWithTitle:title object:nil icon:icon tag:tag];
}

- (id)initWithTitle:(NSString *)title object:(id)object icon:(UIImage *)icon tag:(NSInteger)tag {
    self = [super init];
    if (self) {
        self.title = title;
        self.object = object;
        self.icon = icon;
        self.tag = tag;
    }
    return self;
}

@end
