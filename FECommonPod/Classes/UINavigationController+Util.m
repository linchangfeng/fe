//
//  UINavigationController+Util.m
//  Account
//
//  Created by Tracy on 16/2/1.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import "UINavigationController+Util.h"

@implementation UINavigationController (Util)

- (id)rootViewController {
    return [self.viewControllers firstObject];
}

@end
