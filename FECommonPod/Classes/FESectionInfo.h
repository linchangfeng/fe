//
//  SectionInfo.h
//  memo
//
//  Created by Tracy on 15/1/23.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FECellInfo.h"

@interface FESectionInfo : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) NSInteger tag;
@property (nonatomic, strong) NSArray<FECellInfo*> *items;
@property (nonatomic, strong) id object;
@property (nonatomic, strong) NSArray<UITableViewCell*> *cells;
@property (nonatomic, strong) NSDictionary *userInfo;

+ (id)infoWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items;
+ (id)infoWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items tag:(NSInteger)tag;
+ (id)infoWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells;
+ (id)infoWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells tag:(NSInteger)tag;

- (id)initWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items;
- (id)initWithTitle:(NSString*)title items:(NSArray<FECellInfo*>*)items tag:(NSInteger)tag;
- (id)initWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells;
- (id)initWithTitle:(NSString*)title cells:(NSArray<UITableViewCell*>*)cells tag:(NSInteger)tag;

@end
